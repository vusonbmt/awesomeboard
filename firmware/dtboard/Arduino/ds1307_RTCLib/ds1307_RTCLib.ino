// Date and time functions using a DS1307 RTC connected via I2C and Wire lib
#include <Wire.h>
#include "RTClib.h"
#include <OLED_I2C.h>
OLED  myOLED(PB11, PB10);
RTC_DS1307 rtc;
extern uint8_t SmallFont[];
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

void setup () {
 // while (!Serial1); // for Leonardo/Micro/Zero

  Serial1.begin(115200);
  Serial1.println("test RTC");
  if (! rtc.begin()) {
    Serial1.println("Couldn't find RTC");
    while (1);
  }

  if (! rtc.isrunning()) {
    Serial1.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2018, 10, 24, 7, 25, 0));
  }
  //rtc.adjust(DateTime(2018, 10, 24, 7, 27, 0));
}

void loop () {
    DateTime now = rtc.now();
    
    Serial1.print(now.year(), DEC);
    Serial1.print('/');
    Serial1.print(now.month(), DEC);
    Serial1.print('/');
    Serial1.print(now.day(), DEC);
    Serial1.print(" (");
    Serial1.print(daysOfTheWeek[now.dayOfTheWeek()]);
    Serial1.print(") ");
    Serial1.print(now.hour(), DEC);
    Serial1.print(':');
    Serial1.print(now.minute(), DEC);
    Serial1.print(':');
    Serial1.print(now.second(), DEC);
    Serial1.println();
    
    Serial1.print(" since midnight 1/1/1970 = ");
    Serial1.print(now.unixtime());
    Serial1.print("s = ");
    Serial1.print(now.unixtime() / 86400L);
    Serial1.println("d");
    
    // calculate a date which is 7 days and 30 seconds into the future
    DateTime future (now + TimeSpan(7,12,30,6));
    
    Serial1.print(" now + 7d + 30s: ");
    Serial1.print(future.year(), DEC);
    Serial1.print('/');
    Serial1.print(future.month(), DEC);
    Serial1.print('/');
    Serial1.print(future.day(), DEC);
    Serial1.print(' ');
    Serial1.print(future.hour(), DEC);
    Serial1.print(':');
    Serial1.print(future.minute(), DEC);
    Serial1.print(':');
    Serial1.print(future.second(), DEC);
    Serial1.println();
    
    Serial1.println();
   
    delay(1000);
}
