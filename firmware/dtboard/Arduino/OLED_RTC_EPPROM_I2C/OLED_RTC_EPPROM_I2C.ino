// OLED_I2C_Scrolling_Text
// Copyright (C)2015 Rinky-Dink Electronics, Henning Karlsen. All right reserved
// web: http://www.RinkyDinkElectronics.com/
//
// A quick demo of how to use my OLED_I2C library.
//
// To use the hardware I2C (TWI) interface of the Arduino you must connect
// the pins as follows:
//
// Arduino Uno/2009:
// ----------------------
// Display:  SDA pin   -> Arduino Analog 4 or the dedicated SDA pin
//           SCL pin   -> Arduino Analog 5 or the dedicated SCL pin
//
// Arduino Leonardo:
// ----------------------
// Display:  SDA pin   -> Arduino Digital 2 or the dedicated SDA pin
//           SCL pin   -> Arduino Digital 3 or the dedicated SCL pin
//
// Arduino Mega:
// ----------------------
// Display:  SDA pin   -> Arduino Digital 20 (SDA) or the dedicated SDA pin
//           SCL pin   -> Arduino Digital 21 (SCL) or the dedicated SCL pin
//
// Arduino Due:
// ----------------------
// Display:  SDA pin   -> Arduino Digital 20 (SDA) or the dedicated SDA1 (Digital 70) pin
//           SCL pin   -> Arduino Digital 21 (SCL) or the dedicated SCL1 (Digital 71) pin
//
// The internal pull-up resistors will be activated when using the 
// hardware I2C interfaces.
//
// You can connect the OLED display to any available pin but if you use 
// any other than what is described above the library will fall back to
// a software-based, TWI-like protocol which will require exclusive access 
// to the pins used, and you will also have to use appropriate, external
// pull-up resistors on the data and clock signals.
//

#include <OLED_I2C.h>
#include "uRTCLib.h"
OLED  myOLED(PB11, PB10);
uRTCLib rtc(0x68, 0x50);
extern uint8_t SmallFont[];
unsigned int pos;
#define USE_I2C2 
#ifdef USE_I2C2
extern HardWire HWire;
#endif
void setup()
{
  
  delay(100);
  HWire.begin();
  Serial1.begin(115200);
  Serial1.println("Serial1 OK");
  #ifdef _VARIANT_ARDUINO_STM32_
    Serial1.println("Board: STM32");
  #else
    Serial1.println("Board: Other");
  #endif
#ifdef ARDUINO_ARCH_AVR
  int inttmp = 32123;
#else
  // too logng for AVR 16 bits!
  int inttmp = 24543557;
#endif
  float floattmp = 3.1416;
  char chartmp = 'A';

  // Testing template
  if (!rtc.eeprom_write(0, inttmp)) {
    Serial1.println("Failed to store INT");
  } else {
    Serial1.println("INT correctly stored");
  }
  if (!rtc.eeprom_write(4, floattmp)) {
    Serial1.println("Failed to store FLOAT");
  } else {
    Serial1.println("FLOAT correctly stored");
  }
  if (!rtc.eeprom_write(8, chartmp)) {
    Serial1.println("Failed to store CHAR");
  } else {
    Serial1.println("CHAR correctly stored");
  }

  rtc.set(0, 42, 16, 6, 2, 5, 15);
  //  RTCLib::set(byte second, byte minute, byte hour, byte dayOfWeek, byte dayOfMonth, byte month, byte year)


  inttmp = 0;
  floattmp = 0;
  chartmp = 0;



  Serial1.print("int: ");
  rtc.eeprom_read(0, &inttmp);
  Serial1.println(inttmp);
  Serial1.print("float: ");
  rtc.eeprom_read(4, &floattmp);
  Serial1.println((float) floattmp);
  Serial1.print("char: ");
  rtc.eeprom_read(8, &chartmp);
  Serial1.println(chartmp);
  Serial1.println();


  /*for(pos = 9; pos < 1000; pos++) {
    rtc.eeprom_write(pos, (unsigned char) (pos % 256));
  }*/

  pos = 0;
  myOLED.begin();
  myOLED.setFont(SmallFont);
  randomSeed(analogRead(0));
  
}

void loop()
{
  HWire.begin();
  rtc.refresh();

  Serial1.print("RTC DateTime: ");
  Serial1.print(rtc.year());
  Serial1.print('/');
  Serial1.print(rtc.month());
  Serial1.print('/');
  Serial1.print(rtc.day());

  Serial1.print(' ');

  Serial1.print(rtc.hour());
  Serial1.print(':');
  Serial1.print(rtc.minute());
  Serial1.print(':');
  Serial1.print(rtc.second());

  Serial1.print(" DOW: ");
  Serial1.print(rtc.dayOfWeek());

  Serial1.print(" - Temp: ");
  Serial1.print(rtc.temp());

  Serial1.print(" ---- ");
  Serial1.print(pos);
  Serial1.print(": ");
  Serial1.print(rtc.eeprom_read(pos));

  Serial1.println();

  pos++;
  pos %= 1000;
  myOLED.begin();
  myOLED.setFont(SmallFont);
  int y = random(0, 56);
  for (int i=128; i>=-(34*6); i--)
  {
    myOLED.print("OLED_I2C Scrolling Text Demo ", i, y);
    myOLED.update();
    delay(10);
  }
  delay(1000);
}


