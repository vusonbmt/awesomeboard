#include "Arduino.h"
#include "Wire.h"
#include "uRTCLib.h"
#define USE_I2C2 
#ifdef USE_I2C2
extern HardWire HWire;
#endif
// uRTCLib rtc;
uRTCLib rtc(0x68, 0x50);



unsigned int pos;

void setup() {
  delay(1000);
	Serial1.begin(115200);
	Serial1.println("Serial1 OK");
	//  Max position: 32767

	#ifdef USE_I2C2
    HWire.begin();
    Serial1.println("Serial11 OK");
	#else
    Wire.begin();
	#endif


	#ifdef _VARIANT_ARDUINO_STM32_
		Serial1.println("Board: STM32");
	#else
		Serial1.println("Board: Other");
	#endif
#ifdef ARDUINO_ARCH_AVR
	int inttmp = 32123;
#else
	// too logng for AVR 16 bits!
	int inttmp = 24543557;
#endif
	float floattmp = 3.1416;
	char chartmp = 'A';

	// Testing template
	if (!rtc.eeprom_write(0, inttmp)) {
		Serial1.println("Failed to store INT");
	} else {
		Serial1.println("INT correctly stored");
	}
	if (!rtc.eeprom_write(4, floattmp)) {
		Serial1.println("Failed to store FLOAT");
	} else {
		Serial1.println("FLOAT correctly stored");
	}
	if (!rtc.eeprom_write(8, chartmp)) {
		Serial1.println("Failed to store CHAR");
	} else {
		Serial1.println("CHAR correctly stored");
	}

	rtc.set(0, 42, 16, 6, 2, 5, 15);
	//  RTCLib::set(byte second, byte minute, byte hour, byte dayOfWeek, byte dayOfMonth, byte month, byte year)


	inttmp = 0;
	floattmp = 0;
	chartmp = 0;



	Serial1.print("int: ");
	rtc.eeprom_read(0, &inttmp);
	Serial1.println(inttmp);
	Serial1.print("float: ");
	rtc.eeprom_read(4, &floattmp);
	Serial1.println((float) floattmp);
	Serial1.print("char: ");
	rtc.eeprom_read(8, &chartmp);
	Serial1.println(chartmp);
	Serial1.println();


	for(pos = 9; pos < 1000; pos++) {
		rtc.eeprom_write(pos, (unsigned char) (pos % 256));
	}

	pos = 0;
}

void loop() {
	rtc.refresh();

	Serial1.print("RTC DateTime: ");
	Serial1.print(rtc.year());
	Serial1.print('/');
	Serial1.print(rtc.month());
	Serial1.print('/');
	Serial1.print(rtc.day());

	Serial1.print(' ');

	Serial1.print(rtc.hour());
	Serial1.print(':');
	Serial1.print(rtc.minute());
	Serial1.print(':');
	Serial1.print(rtc.second());

	Serial1.print(" DOW: ");
	Serial1.print(rtc.dayOfWeek());

	Serial1.print(" - Temp: ");
	Serial1.print(rtc.temp());

	Serial1.print(" ---- ");
	Serial1.print(pos);
	Serial1.print(": ");
	Serial1.print(rtc.eeprom_read(pos));

	Serial1.println();

	pos++;
	pos %= 1000;
	delay(1000);
}
