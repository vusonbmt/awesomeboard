/******************** (C) COPYRIGHT 2018 HocARM.org ********************

* Description        : Blink LED with push button
*/
/* Includes ------------------------------------------------------------------*/
#include <stm32f10x.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
void Delay(uint32_t nTime);
int main(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    // Enable Peripheral Clocks
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    // Configure LED PA1
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // Confgiure pushbutton PA8
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    // Configure SysTick Timer
    if (SysTick_Config(SystemCoreClock / 1000))
        while (1)
            ;

    while (1)
    {
        // static int ledVal = 0;
        uint8_t butVal;

        // Toggle green LED
        // GPIO_WriteBit(GPIOA, GPIO_Pin_1, ledVal ? Bit_SET : Bit_RESET);
        // ledVal = 1 - ledVal;

        // Read push button
        butVal = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_8);

        // Toggle blue LED
        GPIO_WriteBit(GPIOA, GPIO_Pin_1, butVal ? Bit_SET : Bit_RESET);

        Delay(250); // wait 250ms
    }
}
// Timer code
static __IO uint32_t TimingDelay;
void Delay(uint32_t nTime)
{
    TimingDelay = nTime;
    while (TimingDelay != 0)
        ;
}
void SysTick_Handler(void)
{
    if (TimingDelay != 0x00)
        TimingDelay--;
}
#ifdef USE_FULL_ASSERT
void assert_failed(uint8_t *file, uint32_t line)
{
    /* Infinite loop */
    /* Use GDB to find out why we're here */
    while (1)
        ;
}
#endif

/******************* (C) COPYRIGHT 2018 HocARM.org *****END OF FILE****/
