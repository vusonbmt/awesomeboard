/******************** (C) COPYRIGHT 2018 HocARM.org ********************

* Description        : Blink LED with push button
*/
/* Includes ------------------------------------------------------------------*/
#include <stm32f10x.h>
int main(void)
{
	// Enable Peripherals Clocks
	RCC->AHBENR |= ((1UL << 17) | (1UL << 19));
	
	// Configure Pins
	
	// 1. LED
	GPIOC->CRL |= (1UL << 2 * 8);

	// 2. Push button
	// Nothing to do ...
	
	while(1)
	{
		uint32_t butVal;
		
		// Read push button
		butVal = GPIOA->IDR & (1 << 0);
		
		// Toggle blue LED
		GPIOC->BSRR = butVal ? (1 << 8) : (1 << 24);
	}
}


/******************* (C) COPYRIGHT 2018 HocARM.org *****END OF FILE****/
